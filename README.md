# Group Project C7
## Advance Programming 2020/2021
[![pipeline status](https://gitlab.com/adpro-c7/turn-based-game/badges/master/pipeline.svg)](https://gitlab.com/adpro-c7/turn-based-game/-/commits/master/)
[![coverage report](https://gitlab.com/adpro-c7/turn-based-game/badges/master/coverage.svg)](https://gitlab.com/adpro-c7/turn-based-game/-/commits/master/)

# Turn-based Game
Web game where we can fight enemies.

### Repository:
[https://gitlab.com/adpro-c7](https://gitlab.com/adpro-c7)

### Deployed Site:
[https://c7-turnbasedgame.herokuapp.com/](https://c7-turnbasedgame.herokuapp.com/)

### Patterns Implemented:
- Command Pattern
- Observer Pattern
- Strategy Pattern