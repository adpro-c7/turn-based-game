package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.service;


import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;



public interface GameService {


    Iterable<Chara> getHeroes(String token);
    Iterable<Chara> getEnemies(String token);

    String attack(String token, String user, String target);
    String skill(String token, String user);

    String getState(String token);

    Iterable<String> getLogs(String token);

}
