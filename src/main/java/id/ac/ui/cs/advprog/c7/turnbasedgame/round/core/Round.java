package id.ac.ui.cs.advprog.c7.turnbasedgame.round.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameSimulator;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameState;

public class Round implements GameSimulatorObserver{

    private int currentRound;
    private int maxRound;
    private GameSimulator gameSimulator;

    public Round(){}

    public void addRound(){
        this.currentRound = this.currentRound + 1;
    }

    public void resetRound(){
        this.currentRound = 0;
    }

    // TODO : Nanti gameSimulator ini jadi parameter constructor / setGameSimulator disaat save()
    public void setGameSimulator(GameSimulator gameSimulator){
        this.gameSimulator = gameSimulator;
    }

    public void setNewMaxRound(){
        if (getCurrentRound() > getMaxRound()){
            this.maxRound = getCurrentRound();
        }
    }

    @Override
    public void update(){
        if (getCurrentGameState() == GameState.WIN){
            addRound();
        } else if (getCurrentGameState() == GameState.LOSE){
            setNewMaxRound();
            resetRound();
        }
    }

    public int getCurrentRound(){
        return this.currentRound;
    }

    public int getMaxRound(){
        return this.maxRound;
    }

    public GameSimulator getGameSimulator(){
        return this.gameSimulator;
    }

    public GameState getCurrentGameState(){
        return gameSimulator.getState();
    }

}
