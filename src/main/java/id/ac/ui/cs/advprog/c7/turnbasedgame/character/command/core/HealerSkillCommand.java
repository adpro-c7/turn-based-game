package id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;

public class HealerSkillCommand extends SkillCommand{

    public HealerSkillCommand(Chara user) {
        super(user, "hero");
    }

    @Override
    public String execute() {
        for(Chara target: targets) {
            target.attacked(user.getAttack() / 3);
        }
        return user.getName() + " use skill : " + user.skill();
    }

    @Override
    public String commandName() {
        return "Healer skill";
    }

    @Override
    public void setTarget(Chara target) {
        this.targets.add(target);
    }
}
