package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.repository;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LogRepositoryImpl implements LogRepository {

    private List<String> logs = new ArrayList<>();

    @Override
    public List<String> findAll() {
        return logs;
    }

    @Override
    public void addLog(String newLog) {
        logs.add(newLog);
    }

    @Override
    public List<String> reset() {
        return logs = new ArrayList<>();
    }
}


