package id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core.AttackCommand;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core.Command;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core.SkillCommand;
import java.util.HashMap;

public class CommandRepository {

    private HashMap<String, AttackCommand> attackCommands;
    private HashMap<String, SkillCommand> skillCommands;

    public CommandRepository() {
        attackCommands = new HashMap<>();
        skillCommands = new HashMap<>();
    }

    public AttackCommand getAttackCommand(String name) {
        return attackCommands.get(name);
    }

    public SkillCommand getSkillCommand(String name) {
        return skillCommands.get(name);
    }

    public void registerAttackCommand(AttackCommand command) {
        attackCommands.put(command.commandName(), command );
    }

    public void registerSkillCommand(SkillCommand command, Iterable<Chara> targets) {
        command.replaceTargets(targets);
        skillCommands.put(command.commandName(), command );
    }

    public String executeAttackCommand(String commandName, Chara target) {
        Command command = getAttackCommand(commandName);
        command.setTarget(target);
        return command.execute();
    }

    public String executeSkillCommand(String commandName) {
        Command command = getSkillCommand(commandName);
        return command.execute();
    }

    public void resetCommandTarget(Iterable<Chara> newHeroes, Iterable<Chara> newEnemies) {
        for (SkillCommand command: skillCommands.values()) {
            if (command.getTargetType().equalsIgnoreCase("hero")) {
                command.replaceTargets(newHeroes);
            } else {
                command.replaceTargets(newEnemies);
            }
        }
    }
}
