package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel;

import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.GameSimulatorObserver;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameSimulator;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameState;

import java.util.*;

@Service
public class PlayerLevelServiceImpl implements PlayerLevelService, GameSimulatorObserver {

    private int maxExperience = 500;
    private int experience;
    private int playerLevel = 1;
    private GameSimulator gameSimulator;

    @Override
    public void setLevel() {
        if (experience >= maxExperience) {
            playerLevel += 1;
        }
    }

    @Override
    public int getLevel() {
        return playerLevel;
    }

    @Override
    public void update() {
        if (getCurrentGameState() == GameState.WIN){
            getLevel();
        } else if (getCurrentGameState() == GameState.LOSE){
            playerLevel = 1;
        }
    }

    public GameSimulator getGameSimulator(){
        return this.gameSimulator;
    }

    public GameState getCurrentGameState(){
        return gameSimulator.getState();
    }
}
