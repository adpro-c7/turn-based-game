package id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice;

import id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round;
import java.util.List;

public interface RoundService {
    public int findHighScore();
    public Round findByToken(String token);
    public List<Round> findAll();
}
