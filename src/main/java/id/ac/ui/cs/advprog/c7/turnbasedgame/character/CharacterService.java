package id.ac.ui.cs.advprog.c7.turnbasedgame.character;

import java.util.Collection;

public interface CharacterService {
    void attack(String user, String target);
    void skill(String user);
    Collection<Chara> getHeroes();
    Iterable<Chara> getEnemies();
    void resetStatsAndEnemy();
    void enemyAttack();
    String checkStatus();
    Chara getChara(String name);
}
