package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameFactory;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameSimulator;

public interface GameRepository {

    GameSimulator getGameSimulatorByToken(String token);
    String deleteGameSimulatorByToken(String token);
    GameSimulator registerGameSimulatorByToken(String token, GameSimulator gameSimulator);
    GameFactory getGameFactory();
}
