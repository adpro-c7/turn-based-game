package id.ac.ui.cs.advprog.c7.turnbasedgame.frontend;

import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel.PlayerLevelService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice.RoundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    private LogService logService;

    @Autowired
    private PlayerLevelService playerLevelService;
//    private GamePlayService gamePlayService;
    @Autowired
    private RoundService roundService;

    @RequestMapping(value = "/homepage", method = RequestMethod.GET)
    public String homepage(Model model) {
        model.addAttribute("highscore", roundService.findHighScore());
        return "homepage";
    }

    @RequestMapping(value = "/initGame", method = RequestMethod.GET)
    public String initGame(Model model) {
        logService.battleLog();
        List<String> allLog = logService.getAllLog();
        model.addAttribute("battleLog", allLog);
        int currentLevel = playerLevelService.getLevel();
        model.addAttribute("playerLevel", currentLevel);
//        gamePlayService = new GamePlayService();
//        gamePlayService.start();
        return "game";
    }

}