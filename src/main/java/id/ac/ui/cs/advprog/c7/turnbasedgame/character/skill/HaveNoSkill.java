package id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill;

public class HaveNoSkill implements Skill{
    @Override
    public String useSkill() {
        return "Do nothing";
    }
}
