package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.repository;

import java.util.List;

public interface LogRepository {

    List<String> findAll();
    void addLog(String log);
    List<String> reset();
}
