package id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;

public class KnightSkillCommand extends SkillCommand{

    public KnightSkillCommand(Chara user) {
        super(user, "enemy");
    }

    @Override
    public String execute() {
        for(Chara target: targets) {
            target.attacked(user.getAttack() / 3);
        }
        return user.getName() + " use skill : " + user.skill();
    }

    @Override
    public String commandName() {
        return "Knight skill";
    }

    @Override
    public void setTarget(Chara target) {
        this.targets.add(target);
    }
}
