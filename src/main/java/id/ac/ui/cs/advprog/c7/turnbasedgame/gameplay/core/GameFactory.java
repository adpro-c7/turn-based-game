package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core;

public interface GameFactory {
    GameSimulator createGame();
}
