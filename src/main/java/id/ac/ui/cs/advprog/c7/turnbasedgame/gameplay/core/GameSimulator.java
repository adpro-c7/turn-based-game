package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core;
import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameState;

public interface GameSimulator {
    // Belum jadi
    String attack(String user, String target);
    String skill(String user);
    GameState getState();
    void setState(GameState gameState);
}
