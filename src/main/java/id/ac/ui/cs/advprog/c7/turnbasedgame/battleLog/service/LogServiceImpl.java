package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service;


import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogServiceImpl implements LogService{

    @Autowired
    private LogRepository logRepository;
    private String allLog;

    @Override
    public void battleLog() {
        logRepository.addLog("test log");
        logRepository.addLog("test log 2");
        logRepository.addLog("test scroll log 1");
        logRepository.addLog("test scroll log 2");
    }

    public List<String> getAllLog() {
        return logRepository.findAll();
    }

    public void clearLog() {
        logRepository.reset();
    }
}
