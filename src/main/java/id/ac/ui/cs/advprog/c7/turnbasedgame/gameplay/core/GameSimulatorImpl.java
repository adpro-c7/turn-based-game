package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.CharacterService;

public class GameSimulatorImpl implements GameSimulator {
    CharacterService characterService;
    GameState gameState;

    public GameSimulatorImpl(CharacterService characterService) {
        this.characterService = characterService;
        this.gameState = GameState.PLAYER_TURN;
    }

    @Override
    public String attack(String user, String target) {
        characterService.attack(user, target);
        return "placeholder";
    }

    @Override
    public String skill(String user) {
        characterService.skill(user);
        return "placeholder";
    }

    @Override
    public GameState getState() {
        return gameState;
    }

    @Override
    public void setState(GameState gameState) {
        this.gameState = gameState;
    }

}
