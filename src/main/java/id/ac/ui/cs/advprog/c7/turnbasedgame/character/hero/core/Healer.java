package id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill.HealSkill;

public class Healer extends Chara {

    public Healer(int maxHealth, int maxSkillUsage, int attack) {
        super(maxHealth, maxSkillUsage, attack, "Healer");
        setSkill(new HealSkill());
    }

    @Override
    public String attack() {
        return "healer attack";
    }
}
