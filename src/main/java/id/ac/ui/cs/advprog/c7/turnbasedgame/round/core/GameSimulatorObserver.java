package id.ac.ui.cs.advprog.c7.turnbasedgame.round.core;

public interface GameSimulatorObserver {
    public void update();
}
