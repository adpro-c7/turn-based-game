package id.ac.ui.cs.advprog.c7.turnbasedgame.playerLevel;

public interface PlayerLevelService {
//    public void addListMaxRound();
    public void setLevel();
    public int getLevel();
}
