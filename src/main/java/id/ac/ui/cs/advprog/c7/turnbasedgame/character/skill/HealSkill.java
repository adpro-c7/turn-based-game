package id.ac.ui.cs.advprog.c7.turnbasedgame.character.skill;

public class HealSkill implements Skill{
    @Override
    public String useSkill() {
        return "Healed all heroes";
    }
}
