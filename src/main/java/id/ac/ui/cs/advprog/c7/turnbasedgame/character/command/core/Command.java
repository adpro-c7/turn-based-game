package id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.core;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara;

public interface Command {
    String execute();
    String commandName();
    void setTarget(Chara target);
}
