package id.ac.ui.cs.advprog.c7.turnbasedgame.character;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.service.CommandService;
import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.service.CommandServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

import java.util.ArrayList;
import java.util.Random;

public class CharacterServiceImpl implements CharacterService {

    @Autowired
    CommandService commandService;
    @Autowired
    CharacterRepository characterRepository;

    public CharacterServiceImpl() {
        characterRepository = new CharacterRepository();
        commandService = new CommandServiceImpl(characterRepository);
    }

    @Override
    public void attack(String user, String target) {
        Chara targetChara = characterRepository.getCharacter(target);
        commandService.attack(user , targetChara);
    }

    @Override
    public void skill(String user) {
        commandService.skill(user);
    }

    @Override
    public Collection<Chara> getHeroes() {
        return characterRepository.getHeroes();
    }

    @Override
    public Iterable<Chara> getEnemies() {
        return characterRepository.getEnemies();
    }

    @Override
    public void resetStatsAndEnemy() {
        characterRepository.resetStatsAndEnemy();
        commandService.resetTargets();
    }

    @Override
    public void enemyAttack() {
        String enemy = randomEnemy().getName();
        String hero = randomHero().getName();

        this.attack(enemy, hero);
    }

    @Override
    public String checkStatus() {
        return characterRepository.checkStatus();
    }

    @Override
    public Chara getChara(String name) {
        return characterRepository.getCharacter(name);
    }

    private Chara randomEnemy() {
        ArrayList<Chara> enemies = new ArrayList<>();
        Random random = new Random();
        int selectEnemy = 0;

        for(Chara enemy : characterRepository.getEnemies()) {
            enemies.add(enemy);
        }

        do {
            selectEnemy = random.nextInt()%3;
        } while (!enemies.get(selectEnemy).isAlive());

        return enemies.get(selectEnemy);
    }

    private Chara randomHero() {
        ArrayList<Chara> heroes = new ArrayList<>();
        Random random = new Random();
        int selectHero = 0;

        for(Chara hero : characterRepository.getHeroes()) {
            heroes.add(hero);
        }

        do {
            selectHero = random.nextInt()%3;
        } while (!heroes.get(selectHero).isAlive());

        return heroes.get(selectHero);
    }
}
