package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service;

import java.util.ArrayList;
import java.util.List;

public interface LogService {
    void battleLog();
    List<String> getAllLog();
    void clearLog();
}
