package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Repository
public class LogRepositoryTest {
    private LogRepository logRepository;

    @Mock
    private List<String> logs;

    @BeforeEach
    public void setUp() {
        logRepository = new LogRepositoryImpl();
        logs = new ArrayList<>();
        logs.add("test log");
    }
}
