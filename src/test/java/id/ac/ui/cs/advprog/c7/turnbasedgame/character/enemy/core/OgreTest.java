package id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class OgreTest {

    private Class<?> ogre;

    @BeforeEach
    public void setUp() throws Exception {
        ogre = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core.Ogre");
    }

    @Test
    public void testOgreIsConcreteClass() {
        assertFalse(Modifier.isAbstract(ogre.getModifiers()));
    }

    @Test
    public void testOgreIsAnChara() {
        Class<?> parentClass = ogre.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara",
                parentClass.getName());
    }

    @Test
    public void testOgreAttackMethodReturnsOgreAttack() throws Exception {
        Method getAlias = ogre.getDeclaredMethod("attack");

        assertEquals("java.lang.String", getAlias.getGenericReturnType().getTypeName());
        assertEquals(0, getAlias.getParameterCount());
    }
}
