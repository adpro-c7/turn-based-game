package id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service;


import id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LogServiceTest {
    private Class<?> logServiceClass;

    @Mock
    LogRepository logRepository;

    @BeforeEach
    public void setup() throws Exception {
        logServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.c7.turnbasedgame.battleLog.service.LogServiceImpl");
    }
}
