package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameSimulator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class GameRepositoryTest {

    private GameRepository gameRepository;

    @Mock
    private GameSimulator gameSimulator = mock(GameSimulator.class);

    @BeforeEach
    public void setGameRepository() {
        gameRepository = new GameRepositoryImpl();
    }

    @Test
    public void testGameRepositoryCanRegisterGameSimulator() {
        String mockToken = "MockedToken";
        assertEquals(gameSimulator, gameRepository.registerGameSimulatorByToken(mockToken, gameSimulator));
        assertEquals(gameSimulator, gameRepository.getGameSimulatorByToken(mockToken));
    }

    @Test
    public void testGameRepositoryHasGameFactory() {
        assertNotNull(gameRepository.getGameFactory());
    }

    @Test
    public void testGameRepositoryCanDeleteGame() {
        String mockToken = "MockToken";
        gameRepository.registerGameSimulatorByToken(mockToken, gameSimulator);
        assertEquals(gameSimulator, gameRepository.getGameSimulatorByToken(mockToken));

        gameRepository.deleteGameSimulatorByToken(mockToken);
        assertNull(gameRepository.getGameSimulatorByToken(mockToken));
    }
}
