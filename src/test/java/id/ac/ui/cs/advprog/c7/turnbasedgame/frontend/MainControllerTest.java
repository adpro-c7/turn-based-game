package id.ac.ui.cs.advprog.c7.turnbasedgame.frontend;

import id.ac.ui.cs.advprog.c7.turnbasedgame.round.sevice.RoundService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.security.RunAs;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MainController.class)
public class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RoundService roundService;

//    @Test
//    public void whenHomepageUrlShouldCallRoundService() throws Exception{
//        mockMvc.perform(get("/homepage"))
//                .andExpect(status().isOk())
//                .andExpect(handler().methodName("homepage"))
//                .andExpect(model().attributeExists("highscore"))
//                .andExpect(view().name("homepage"));
//        verify(roundService, times(1)).findHighScore();
//    }

}
