package id.ac.ui.cs.advprog.c7.turnbasedgame.round.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GameSimulatorObserverTest {

    private Class<?> gameSimulatorObserverClass;

    @BeforeEach
    public void setup() throws Exception {
        gameSimulatorObserverClass = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.GameSimulatorObserver");
    }

    @Test
    public void testGameSimulatorObserverIsAPublicInterface() {
        int classModifiers = gameSimulatorObserverClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testBowHasUpdateAbstractMethod() throws Exception {
        Method update = gameSimulatorObserverClass.getDeclaredMethod("update");
        int methodModifiers = update.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, update.getParameterCount());
    }

}
