package id.ac.ui.cs.advprog.c7.turnbasedgame.round.repository;

import id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class RoundRepositoryTest {

    private RoundRepository roundRepository;

    @Mock
    private Map<String, Round> roundMap;

    private Round round;

    @BeforeEach
    public void setUp() {
        roundRepository = new RoundRepository();
        roundMap = new HashMap<>();
        round = new Round();
        roundMap.put("token", round);
    }

    @Test
    public void whenRoundRepoFindAllItShouldReturnRoundMapList() {
        ReflectionTestUtils.setField(roundRepository, "roundMap", roundMap);

        List<Round> acquiredRound = roundRepository.findAll();

        assertThat(acquiredRound).isEqualTo(new ArrayList<>(roundMap.values()));
    }

    @Test
    public void whenRoundRepoFindByTokenItShouldReturnRound() {
        ReflectionTestUtils.setField(roundRepository, "roundMap", roundMap);
        Round acquiredRound = roundRepository.findByToken("token");

        assertThat(acquiredRound).isEqualTo(round);
    }

    @Test
    public void whenRoundRepoSaveItShouldSaveRound() {
        ReflectionTestUtils.setField(roundRepository, "roundMap", roundMap);
        Round newRound = new Round();
        roundRepository.save("token2", newRound);
        Round acquiredNewRound = roundRepository.findByToken("token2");

        assertThat(acquiredNewRound).isEqualTo(newRound);
    }

}
