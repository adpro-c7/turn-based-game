package id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TrollTest {

    private Class<?> troll;

    @BeforeEach
    public void setUp() throws Exception {
        troll = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core.Troll");
    }

    @Test
    public void testTrollIsConcreteClass() {
        assertFalse(Modifier.isAbstract(troll.getModifiers()));
    }

    @Test
    public void testTrollIsAnChara() {
        Class<?> parentClass = troll.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara",
                parentClass.getName());
    }

    @Test
    public void testTrollAttackMethodReturnsTrollAttack() throws Exception {
        Method getAlias = troll.getDeclaredMethod("attack");

        assertEquals("java.lang.String", getAlias.getGenericReturnType().getTypeName());
        assertEquals(0, getAlias.getParameterCount());
    }
}
