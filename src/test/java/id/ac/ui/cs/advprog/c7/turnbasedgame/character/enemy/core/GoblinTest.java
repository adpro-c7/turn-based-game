package id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class GoblinTest {

    private Class<?> goblin;

    @BeforeEach
    public void setUp() throws Exception {
        goblin = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.enemy.core.Goblin");
    }

    @Test
    public void testGoblinIsConcreteClass() {
        assertFalse(Modifier.isAbstract(goblin.getModifiers()));
    }

    @Test
    public void testGoblinIsAnChara() {
        Class<?> parentClass = goblin.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara",
                parentClass.getName());
    }

    @Test
    public void testGoblinAttackMethodReturnsGoblinAttack() throws Exception {
        Method getAlias = goblin.getDeclaredMethod("attack");

        assertEquals("java.lang.String", getAlias.getGenericReturnType().getTypeName());
        assertEquals(0, getAlias.getParameterCount());
    }

}
