package id.ac.ui.cs.advprog.c7.turnbasedgame.character;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.command.service.CommandService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CharacterServiceTest {
    private Class<?> characterServiceClass;

    CharacterService characterService;

    @BeforeEach
    public void setup() throws Exception {
        characterServiceClass = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.CharacterService");
        characterService = new CharacterServiceImpl();
    }

    @Test
    public void testCharacterServiceGetHeroesShouldReturnCorrectly() throws Exception {
        int result = characterService.getHeroes().size();
        assertEquals(3, result);
    }

    @Test
    public void testCharacterServiceAttackShouldDecreaseHealth() throws Exception {
        Collection<Chara> heroes = characterService.getHeroes();
        Chara target = null;
        for(Chara chara: heroes) {
            if (chara.getName().equalsIgnoreCase("Knight")) {
                target = chara;
            }
        }

        characterService.attack("Healer", "Knight");
        int health = target.getCurrentHealth();
        assertEquals(280, health);
    }

    @Test
    public void testCharacterServiceKnightSkillShouldDecreaseAllEnemiesHealth() throws Exception {
        characterService.skill("Knight");
        Iterable<Chara> enemies = characterService.getEnemies();
        for(Chara chara: enemies) {
            assertEquals(chara.getMaxHealth()-10, chara.getCurrentHealth());
        }
    }

    @Test
    public void testResetStatsAndEnemyShouldReset() throws Exception {
        Collection<Chara> heroes = characterService.getHeroes();
        Chara target = null;
        for(Chara chara: heroes) {
            if (chara.getName().equalsIgnoreCase("knight")) {
                target = chara;
            }
        }

        characterService.attack("Healer", "Knight");
        characterService.resetStatsAndEnemy();

        int health = target.getCurrentHealth();
        assertEquals(target.getMaxHealth(), health);
    }

    @Test
    public void testCharacterServiceCheckStatusShouldReturnCorrectly() throws Exception {
        String result = characterService.checkStatus();
        assertEquals("CONTINUE", result);

        for(int i = 0; i < 50; i++) {
            characterService.attack("Healer", "Goblin");
            characterService.attack("Knight", "Ogre");
            characterService.attack("Mage", "Troll");
        }
        result = characterService.checkStatus();
        assertEquals("WIN", result);

        characterService.resetStatsAndEnemy();
        for(int i = 0; i < 50; i++) {
            characterService.attack("Goblin", "Healer");
            characterService.attack("Ogre", "Knight");
            characterService.attack("Troll", "Mage");
        }
        result = characterService.checkStatus();
        assertEquals("LOSE", result);
    }
}
