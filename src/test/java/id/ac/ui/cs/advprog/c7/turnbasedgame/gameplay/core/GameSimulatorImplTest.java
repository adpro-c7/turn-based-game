package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.CharacterService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class GameSimulatorImplTest {

    @Mock
    CharacterService characterService = mock(CharacterService.class);

    GameSimulatorImpl gameSimulator;

    @BeforeEach
    public void setUp() throws Exception {
        this.gameSimulator = new GameSimulatorImpl(characterService);
    }

    @Test
    public void testGameSimulatorAttackCorrectlyImplemented() {
        String mockUser = "user";
        String mockTarget = "target";
        gameSimulator.attack(mockUser, mockTarget);
        verify(characterService, atLeastOnce()).attack(mockUser, mockTarget);
    }

    @Test
    public void testGameSimulatorSkillCorrectlyImplemented() {
        String mockUser = "user";
        gameSimulator.skill(mockUser);
        verify(characterService, atLeastOnce()).skill(mockUser);
    }

    @Test
    public void testGameSimulatorSetState() {
        gameSimulator.setState(GameState.PLAYER_TURN);
        assertEquals(gameSimulator.getState(), GameState.PLAYER_TURN);

        gameSimulator.setState(GameState.ENEMY_TURN);
        assertEquals(gameSimulator.getState(), GameState.ENEMY_TURN);

        gameSimulator.setState(GameState.WIN);
        assertEquals(gameSimulator.getState(), GameState.WIN);

        gameSimulator.setState(GameState.LOSE);
        assertEquals(gameSimulator.getState(), GameState.LOSE);
    }

}
