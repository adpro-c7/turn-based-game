package id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GameFactoryTest {

    GameFactoryImpl gameFactory;

    @BeforeEach
    public void setUp() {
        gameFactory = new GameFactoryImpl();
    }

    @Test
    public void testGameFactoryCanCreate() {
        assertNotNull(gameFactory.createGame());
    }
}
