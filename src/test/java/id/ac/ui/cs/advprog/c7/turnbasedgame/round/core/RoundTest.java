package id.ac.ui.cs.advprog.c7.turnbasedgame.round.core;

import id.ac.ui.cs.advprog.c7.turnbasedgame.gameplay.core.GameSimulator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class RoundTest {

    private Class<?> roundClass;

    @InjectMocks
    private Round round;

    private GameSimulator gameSimulator;

    @BeforeEach
    public void setUp() throws Exception {
        roundClass = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.Round");
        round = new Round();
        round.addRound();
        round.setNewMaxRound();
    }

    @Test
    public void testRoundIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(roundClass.getModifiers()));
    }

    @Test
    public void testRoundIsAGameSimulatorObserver() {
        Collection<Type> interfaces = Arrays.asList(roundClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.c7.turnbasedgame.round.core.GameSimulatorObserver")));
    }

    @Test
    public void testRoundOverrideUpdateMethod() throws Exception {
        Method update = roundClass.getDeclaredMethod("update");

        assertEquals("void",
                update.getGenericReturnType().getTypeName());
        assertEquals(0,
                update.getParameterCount());
        assertTrue(Modifier.isPublic(update.getModifiers()));
    }

    @Test
    public void testAddRoundMethodReturnCorrectAnswer() throws Exception{
        round.addRound();
        assertEquals(2, round.getCurrentRound());
    }

    @Test
    public void testResetRoundMethodReturnCorrectAnswer() throws Exception{
        round.resetRound();
        assertEquals(0, round.getCurrentRound());
    }

    @Test
    public void testGetCurrentRoundReturnCorrectAnswer() throws Exception{
        assertEquals(1, round.getCurrentRound());
    }

    @Test
    public void testGetMaxRoundReturnCorrectAnswer() throws Exception{
        round.addRound();
        round.setNewMaxRound();
        assertEquals(2, round.getMaxRound());
    }

    @Test
    public void testSetNewMaxRoundMethodReturnCorrectAnswer() throws Exception{
        for (int i=0; i<10; i++){
            round.addRound();
        }
        round.setNewMaxRound();
        assertEquals(11, round.getMaxRound());

        round.resetRound();
        round.addRound();
        round.setNewMaxRound();
        assertEquals(11, round.getMaxRound());
    }

    // TODO: buat test untuk hasil return dari method setGameSimulator, update, getGameSimulator, getCurrentGameState

}
