package id.ac.ui.cs.advprog.c7.turnbasedgame.character;

import id.ac.ui.cs.advprog.c7.turnbasedgame.character.hero.core.Healer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CharaTest {
    private Class<?> charaClass;

    @Mock
    Chara chara;

    @BeforeEach
    public void setup() throws Exception {
        charaClass = Class.forName("id.ac.ui.cs.advprog.c7.turnbasedgame.character.Chara");
        chara = new Healer(15,1,15);
    }

    @Test
    public void testCharaIsAbstractClass() {
        assertTrue(Modifier.isAbstract(charaClass.getModifiers()));
    }

    @Test
    public void testCharaAttackIsAbstractMethod() throws Exception{
        Method method = charaClass.getDeclaredMethod("attack");
        assertTrue(Modifier.isAbstract(method.getModifiers()));
    }

    @Test
    public void testCharaGetNameShouldReturnCorrectly() throws Exception {
        String result = chara.getName();
        assertEquals("Healer", result);
    }

    @Test
    public void testCharaGetAttackShouldReturnCorrectly() throws Exception {
        int result = chara.getAttack();
        assertEquals(15, result);
    }

    @Test
    public void testCharaGetMaxHealthShouldReturnCorrectly() throws Exception {
        int result = chara.getMaxHealth();
        assertEquals(15, result);
    }

    @Test
    public void testCharaGetMaxSkillUsageShouldReturnCorrectly() throws Exception {
        int result = chara.getMaxSkillUsage();
        assertEquals(1, result);
    }

    @Test
    public void testCharaIsAliveShouldReturnCorrectly() throws Exception {
        boolean result = chara.isAlive();
        assertTrue(result);

        chara.setIsAlive(false);
        result = chara.isAlive();
        assertFalse(result);
    }

    @Test
    public void testCharaAttackedShouldDecreaseHealth() throws Exception {
        int health = chara.getCurrentHealth();
        assertEquals(15, health);

        chara.attacked(15);
        health = chara.getCurrentHealth();
        assertEquals(0, health);
        assertFalse(chara.isAlive());
    }

    @Test
    public void testCharaHealShouldIncreaseHealth() throws Exception {
        chara.attacked(5);
        chara.heal(10);

        int health = chara.getCurrentHealth();
        assertEquals(chara.getMaxHealth(), health);
    }

    @Test
    public void testCharaSkillShouldReturnCorrectly() throws Exception {
        String result = chara.skill();
        int skillUsage = chara.getCurrentSkillUsage();
        assertEquals("Healed all heroes", result);
        assertEquals(0, skillUsage);

        result = chara.skill();
        assertEquals("Cannot use skill!", result);
    }
}
